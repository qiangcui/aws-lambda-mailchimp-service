import Mailchimp from 'mailchimp-api-v3';
// import Slack from 'node-slack';
// import MD5 from 'md5.js';
import response from './libs/response';
import aws from 'aws-sdk';
import csv from 'csvtojson';

const s3 = new aws.S3();
// const slack = new Slack(process.env.SLACK_HOOKS_URL);
const mailchimp = new Mailchimp(process.env.MAILCHIMP_API_KEY);

// eslint-disable-next-line import/prefer-default-export
export const subscribe = async (event, context, callback) => {

  const bucket = event.Records[0].s3.bucket.name;
  const key = event.Records[0].s3.object.key;

  console.log(bucket);
  console.log(key);

  const params = {
    Bucket: bucket,
    Key: key,
  };

  // get csv file and create stream
  const stream = s3.getObject(params).createReadStream();
  // convert csv file (stream) to JSON format data
  const json = await csv().fromStream(stream);
  console.log(json);

  json.forEach((contact) => {
    const payload = {
      email_address: contact.Email,
      status: 'subscribed',
      merge_fields: {
        FNAME: contact['First name'],
        LNAME: contact['Last name'],
        ORDERS: contact['Total Orders'],
        AMOUNTS: contact['Total Spent'],
      },
      tags: [contact.Tags],
    };
    console.log('payload', payload);
    mailchimp.post(`lists/${process.env.MAILCHIMP_LIST_ID}/members`, payload)
      .then((res) => {
        console.log(res);
        callback(null, response(200, { status: 'success' }));
      })
      .catch((error) => {
        if (error.title === 'Member Exists') {
          console.log(`${error.status} : ${error.detail};`);
          // slack.send({ text: error.detail });
          callback(null, response(400, { status: 'error', message: 'member exists' }));
        } else {
          console.error(error);
          // slack.send({ text: error.detail });
          callback(null, response(500, { status: 'error', message: 'undefined' }));
        }
      });
  });
};
